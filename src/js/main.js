'use strict';

var app = angular.module('app',
    [
        'ngRoute',
        'ngAnimate',
        'sun.scrollable',
        'highcharts-ng',
        'obDateRangePicker',
        'pickadate',
        'ui.timepicker',
        'angular-intro',
        'ui.sortable',
        'monospaced.elastic',
        '720kb.tooltips',
        'ui.select'
    ]);

app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'views/home.html'
        })
        .when('/dashboard', {
            templateUrl: 'views/dashboard.html'
        })
        .when('/create/new', {
            templateUrl: 'views/create_new.html'
        })
        .when('/bot', {
            templateUrl: 'views/bot.html'
        })
        .when('/train', {
            templateUrl: 'views/train.html'
        })
        .when('/conversation2', {
            templateUrl: 'views/conversation2.html'
        })
        .when('/build', {
            templateUrl: 'views/build.html'
        })
        .when('/user', {
            templateUrl: 'views/user.html'
        })
        .when('/pricing', {
            templateUrl: 'views/pricing.html'
        })
        .when('/broadcasting', {
            templateUrl: 'views/broadcasting.html'
        })
        .when('/about', {
            templateUrl: 'views/about.html'
        })
        .otherwise({
            redirectTo: '/'
        });

    // $locationProvider.html5Mode(true);
}]);

app.filter('join', function () {
    return function join(array, separator, prop) {
        if (!Array.isArray(array)) {
            return array; // if not array return original - can also throw error
        }

        return (!!prop ? array.map(function (item) {
            return item[prop];
        }) : array).join(separator);
    };
});

/////////////////////////////////////////////////////////////
app.controller('mainCtrl', ['$scope', '$location', 'dateFilter', function ($scope,$location,dateFilter) {
    $scope.go = function ( path ) {
        $location.path( path );
    };
    
    /**
     all variables
     **/
    $scope.vars = {
        date: dateFilter(new Date(), 'yyyy-MM-dd'),
        someDateObject: new Date(),
        radioItem: 0,
        createBot: '',
        openModalInside: false
    };




    /**
     Intro steps
     **/
    $scope.IntroOptions = {
        steps:[
            {
                element: '#step1',
                intro: "This is the 1 tooltip."
            },
            {
                element: '#step3',
                intro: "This is the 3 tooltip.",
                position: 'top'
            },
            {
                element: '#step4',
                intro: "This is the 4 tooltip.",
                position: 'top'
            }
        ],
        showStepNumbers: false,
        exitOnOverlayClick: true,
        exitOnEsc:true,
        nextLabel: '<strong style="color:green">NEXT!</strong>',
        prevLabel: '<span>Previous</span>',
        skipLabel: 'Exit',
        doneLabel: 'Thanks'
    };
    $scope.ShouldAutoStart = false;
    $scope.startIntroOnlyOnce = function () {
        if($scope.ShouldAutoStart){
            return;
        }
        $scope.ShouldAutoStart = true;
    };

    
    /**
     tabs
     **/
    $scope.tab = 1;
    $scope.setTab = function (tab) {
        $scope.tab = tab;
    };
    $scope.isTab = function (tab) {
        return $scope.tab === tab;
    };


    /**
     modals
     **/
    $scope.openModalN = '';
    $scope.openModal = function (openModalN) {
        $scope.openModalN = openModalN;
    };


    /**
     highlight a current menu item
     **/
    $scope.curPage = function (path) {
        return ($location.path().substr(0, path.length) === path) ? 'active' : '';
    };


    /**
     add card
     **/
    $scope.cards = [];
    $scope.addCard = function () {
        $scope.cards.push({
            img: '',
            title: '',
            sub_title: '',
            url: ''
        })
    };


    /**
     add if condition
     **/
    $scope.connections = [];
    $scope.addConnection = function () {
        $scope.connections.push({
            then_go_to_id: ''
        })
    };
    $scope.removeConnection = function (index) {
        $scope.connections.splice(index, 1)
    };

    
    /**
     dashboard charts
     **/
    $scope.chart1 = {
        options: {
            chart: {
                type: 'pie',
                margin: [0,0,0,0],
                height: 170,
                width: 170

            },
            colors: ['#ff9393', '#f0f0f0'],
            exporting: { enabled: false }
        },
        tooltip: {
            enabled: false
        },
        plotOptions: {
            pie: {
                slicedOffset: 0,
                size: '100%',
                dataLabels: {
                    enabled: false
                }
            }
        },
        title: {
            text: '106',
            align: 'center',
            verticalAlign: 'middle',
            style: {
                fontSize: '34px',
                fontWeight: 'bold',
                color: '#2d5570'
            },
            y: 15

        },
        series: [{
            type: 'pie',
            name: 'Browsers',
            data: [["MSIE",10],[,2]],
            innerSize: '90%',
            showInLegend: false,
            dataLabels: {
                enabled: false
            }
        }]
    };

    $scope.chart2 = {
        options: {
            exporting: { enabled: false }
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            lineWidth: 0,
            minorGridLineWidth: 0,
            lineColor: 'transparent',
            labels: {
                enabled: false
            },
            minorTickLength: 0,
            tickLength: 0
        },
        yAxis: {
            title: {
                text: ''
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: '°C'
        },
        series: [{
            showInLegend: false,
            name: 'Tokyo',
            data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
        }, {
            showInLegend: false,
            name: 'New York',
            data: [0.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5]
        }]
    };

    $scope.chart3 = {
        options: {
            chart: {
                type: 'area'
            },
            colors: ["#f45b5b", "#8085e9", "#8d4654", "#7798BF", "#aaeeee", "#ff0066", "#eeaaee",
                "#55BF3B", "#DF5353", "#7798BF", "#aaeeee"],
            exporting: { enabled: false }
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: ['07:01', '07:01', '07:01', '07:01', '07:01', '07:01', '07:01', '07:01']
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        plotOptions: {
            area: {
                fillOpacity: 0.8,
                backgroundColor:'rgba(255, 255, 255, 0.1)'
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            showInLegend: false,
            name: 'John',
            data: [0, 1, 4, 4, 5, 2, 3, 7]
        }, {
            showInLegend: false,
            name: 'Jane',
            data: [1, 0, 3, 5, 3, 1, 2, 1]
        }]
    };


    /**
     broadcasting
     **/
    $scope.broadcasting = [];
    $scope.addBroadcasting = function () {
        $scope.broadcasting3 = [];
        $scope.bcMess = false;
        $scope.broadcasting.push({id:0})
    };
    $scope.removeBroadcasting = function (index) {
        $scope.broadcasting.splice(index, 1)
    };


    $scope.broadcasting2 = [];
    $scope.addBroadcasting2 = function () {
        $scope.broadcasting3 = [];
        $scope.bcMess = false;
        $scope.broadcasting2.push({id:0})
    };
    $scope.removeBroadcasting2 = function (index) {
        $scope.broadcasting2.splice(index, 1)
    };


    $scope.broadcasting3 = [];
    $scope.addBroadcasting3 = function () {
        $scope.broadcasting = [];
        $scope.broadcasting2 = [];
        $scope.bcMess = false;

        $scope.broadcasting3.push({id:0})
    };
    $scope.removeBroadcasting3 = function (index) {
        $scope.broadcasting3.splice(index, 1)
    };


    $scope.bcMess = false;
    $scope.sendBcMess = function () {
        $scope.broadcasting = [];
        $scope.broadcasting2 = [];
        $scope.broadcasting3 = [];

        $scope.bcMess = !$scope.bcMess;
    };


    $scope.bcWeeksArr = [];
    $scope.toggleWeek = function (week) {
        if($scope.bcWeeksArr.indexOf(week)){
            $scope.bcWeeksArr.push(week);
            return;
        }
        $scope.bcWeeksArr.splice($scope.bcWeeksArr.indexOf(week), 1);
    };


    /**
     build items
     **/
    $scope.blocksArr = [{id: 0},{id: 1}];

    $scope.addNewBox = function(){
        $scope.blocksArr.push({
            id: $scope.blocksArr.length
        })
    };
    $scope.removeThisBox = function (index) {
        $scope.blocksArr.splice(index, 1)
    };


    /**
     chat items
     **/
    $scope.chatMessages = [{id: 0},{id: 1}];
    $scope.sendMessage = function(){
        $scope.chatMessages.push({
            id: $scope.chatMessages.length
        })
    };


    /**
     sortable
     **/
    $scope.sortable_option = {
        //Construct method before sortable code
        construct:function(model){
            console.log('construct');
        },
        //Callback after item is dropped
        stop:function(list, dropped_index){
            console.log('stop');
        }
    };
}]);



app.controller('buildCtrl', ['$scope', function ($scope) {
    $scope.vars = {
        singleSelect: 'first',
        checkImage: false,
        checkImage1: false,
        checkImage2: false,
        checkImage3: false,
        checkImage4: false,
        testMessage: [{
            textarea: '',
            buttons: [{
                id: 0,
                title: '',
                url: '',
                type: 0,
                isActive: false
            }]
        }],
        testBot: [{
            id: 0,
            title: 'New bot',
            gallery:[],
            textCard:[],
            image:[],
            plugin:[]
        }],
        table: {
            body: [
                {link: 'Lorem ipsum dolor.',link_price: 123,holder_image: 'i/robot.png',title_value: 'Lorem ipsum',block_value: 'SOLD OUT'},
                {link: 'Lorem ipsum dolor.',link_price: 123,holder_image: 'i/shoes.jpg',title_value: 'Lorem ipsum',block_value: 'SOLD OUT'},
                {link: 'Lorem ipsum dolor.',link_price: 123,holder_image: 'i/shoes.jpg',title_value: 'Lorem ipsum',block_value: 'SOLD OUT'},
                {link: 'Lorem ipsum dolor.',link_price: 123,holder_image: 'i/shoes.jpg',title_value: 'Lorem ipsum',block_value: 'SOLD OUT'},
                {link: 'Lorem ipsum dolor.',link_price: 123,holder_image: 'i/shoes.jpg',title_value: 'Lorem ipsum',block_value: 'SOLD OUT'},
                {link: 'Lorem ipsum dolor.',link_price: 123,holder_image: 'i/shoes.jpg',title_value: 'Lorem ipsum',block_value: 'SOLD OUT'},
                {link: 'Lorem ipsum dolor.',link_price: 123,holder_image: 'i/shoes.jpg',title_value: 'Lorem ipsum',block_value: 'SOLD OUT'},
                {link: 'Lorem ipsum dolor.',link_price: 123,holder_image: 'i/shoes.jpg',title_value: 'Lorem ipsum',block_value: 'SOLD OUT'},
                {link: 'Lorem ipsum dolor.',link_price: 123,holder_image: 'i/shoes.jpg',title_value: 'Lorem ipsum',block_value: 'SOLD OUT'},
                {link: 'Lorem ipsum dolor.',link_price: 123,holder_image: 'i/shoes.jpg',title_value: 'Lorem ipsum',block_value: 'SOLD OUT'},
                {link: 'Lorem ipsum dolor.',link_price: 123,holder_image: 'i/shoes.jpg',title_value: 'Lorem ipsum',block_value: 'SOLD OUT'},
                {link: 'Lorem ipsum dolor.',link_price: 123,holder_image: 'i/shoes.jpg',title_value: 'Lorem ipsum',block_value: 'SOLD OUT'},
                {link: 'Lorem ipsum dolor.',link_price: 123,holder_image: 'i/shoes.jpg',title_value: 'Lorem ipsum',block_value: 'SOLD OUT'},
                {link: 'Lorem ipsum dolor.',link_price: 123,holder_image: 'i/shoes.jpg',title_value: 'Lorem ipsum',block_value: 'SOLD OUT'},
                {link: 'Lorem ipsum dolor.',link_price: 123,holder_image: 'i/shoes.jpg',title_value: 'Lorem ipsum',block_value: 'SOLD OUT'},
                {link: 'Lorem ipsum dolor.',link_price: 123,holder_image: 'i/shoes.jpg',title_value: 'Lorem ipsum',block_value: 'SOLD OUT'},
            ]
        }
    };
    //testBot not done 



    /**
     Intro steps
     **/
    $scope.buildIntroOptions = {
        steps:[
            {
                element: '#buildStep1',
                intro: "This is the 1 tooltip."
            },
            {
                element: '#buildStep2',
                intro: "This is the 2 tooltip."
            },
            {
                element: '#buildStep3',
                intro: "This is the 3 tooltip."
            },
            {
                element: '#buildStep4',
                intro: "This is the 4 tooltip.",
                position: 'right'
            },
            {
                element: '#buildStep5',
                intro: "This is the 5 tooltip.",
                position: 'right'
            },
            {
                element: '#buildStep6',
                intro: "This is the 6 tooltip.",
                position: 'right'
            },
            {
                element: '#buildStep7',
                intro: "This is the 7 tooltip.",
                position: 'right'
            },
            {
                element: '#buildStep8',
                intro: "This is the 8 tooltip.",
                position: 'right'
            }
        ],
        showStepNumbers: false,
        exitOnOverlayClick: true,
        exitOnEsc:true,
        nextLabel: '<strong style="color:green">NEXT!</strong>',
        prevLabel: '<span>Previous</span>',
        skipLabel: 'Exit',
        doneLabel: 'Thanks'
    };
    $scope.buildShouldAutoStart = false;
    $scope.buildStartIntroOnlyOnce = function () {
        if($scope.buildShouldAutoStart){
            return;
        }
        $scope.buildShouldAutoStart = true;
    };



    $scope.groups = [{
        id:0,
        title: 'New group',
        bots: []
    }];
    $scope.addGroup = function(){
        $scope.groups.push({
            id: $scope.groups.length,
            title: 'New group',
            bots: []
        });
    };
    $scope.removeGroup = function (index) {
        $scope.groups.splice(index, 1);
    };
    
    $scope.addGroupBot = function (groupBot) {
        groupBot.push({
            id: groupBot.length,
            title: 'New bot',
            gallery:[],
            textCard:[],
            image:[],
            plugin:[]
        });
    };
    
    $scope.createBuildItem = function (item, groupBot) {
        switch(item) {
            case 'gallery':
                groupBot.gallery.push({
                    items:[{
                        id: groupBot.gallery.length,
                        title: 'Card item',
                        input: '',
                        url: '',
                        textarea: '',
                        buttons:[]
                    }]
                });
                break;
            case 'textCard':
                groupBot.textCard.push({
                    id: groupBot.textCard.length
                });
                break;
            case 'image':
                groupBot.image.push({
                    id: groupBot.image.length
                });
                break;
            case 'plugin':
                groupBot.plugin.push({
                    id: groupBot.plugin.length
                });
                break;
            default:
                console.log('wtf?');
                break;
        }
    };
    $scope.removeBuildItem = function (item, groupBot) {
        switch(item) {
            case 'gallery':
                groupBot.gallery.length = 0;
                break;
            case 'textCard':
                groupBot.textCard.length = 0;
                break;
            case 'image':
                groupBot.image.length = 0;
                break;
            case 'plugin':
                groupBot.plugin.length = 0;
                break;
            default:
                console.log('wtf?');
                break;
        }
    };

    $scope.addGalleryItem = function(galleryItem){
        galleryItem.items.push({
            id: galleryItem.items.length,
            title: 'Card item',
            input: '',
            url: '',
            textarea: '',
            buttons:[]
        })
    };
    $scope.removeGalleryItem = function(item, index){
        item.splice(index, 1);
    };

    $scope.addButton = function (thisItem) {
        thisItem.buttons.push({
            id: thisItem.id+''+thisItem.buttons.length,
            title: '',
            url: '',
            type: 0,
            isActive: false
        });
    };
    $scope.removeButton = function (thisBtn, index) {
        thisBtn.splice(index, 1);
        thisBtn.forEach(function(item) {
            item.isActive = false
        });
    };

    $scope.groupBot = 101;
    $scope.setBot = function (bot) {
        $scope.groupBot = bot;
    };
    $scope.isBot = function (bot) {
        return $scope.groupBot === bot;
    };



    $scope.btnEdit = function (btn,btnType) {
        console.log(btn,btnType);
        btn.isActive = !btn.isActive;
        btn.type = btnType || btn.type;
    };
    $scope.isBtnActive = function (btn) {
        return btn.isActive;
    };
    
    $scope.sortableOptionsY = {
        axis: 'y'
    };
    $scope.sortableOptionsX = {
        axis: 'x'
    };





    this.availableColors = ['Red','Green','Blue','Yellow','Magenta','Maroon','Umbra','Turquoise'];
    this.colors = ['Blue','Red'];

}]);

/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
app.directive("toggleNext", function ($document) {
    return {
        restrict: "A",
        link: function (scope, element, attr, form) {
            $document.on('click', function (e) {
                if (element !== e.target && !element[0].contains(e.target) && !element.parent()[0].contains(e.target)) {
                    scope.$apply(function () {
                        $(element).removeClass('active');
                        $(element).next().removeClass('active');
                    });
                }
            });

            element.on("click", function () {
                $(element).toggleClass('active');
                $(element).next().toggleClass('active');
            });
        }
    };
});

app.directive("toggleThis", function () {
    return {
        restrict: "A",
        link: function (scope, element, attr, form) {
            element.on("click", function () {
                $(element).toggleClass('active');
            });
        }
    };
});

app.directive('bxslider',function() {
    return {
        restrict: 'A',
        link: function(scope, elm) {
            elm.ready(function() {
                elm.bxSlider({
                    controls: false,
                    autoStart: false,
                    pager: true
                });
            });
        }
    };
});

